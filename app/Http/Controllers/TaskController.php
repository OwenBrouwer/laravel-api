<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function create(Request $request)
    {
        $task = $request->json();

        $name = $task->get('name');
        $description = $task->get('description');
        $done = $task->get('done');
        $tag = $task->get('tag');

        Task::create([
            'name' => $name,
            'description' => $description,
            'done' => $done,
            'tag' => $tag,
        ]);

        return response()->json('Post was succesfull');
    }

    public function getTask()
    {

        $getData = Task::get();

        foreach ($getData as $item) {
            $item->created = $item->created_at->diffForHumans();
        }

        return response()->json($getData);
    }

    public function delete($id)
    {
        Task::where('id', $id)->delete();

        return response()->json('Succesfully deleted the data!');
    }

    public function update(Request $request)
    {
        $new = $request->json();
        $id = $new->get('id');
        $name = $new->get('name');
        $description = $new->get('description');
        $done = $new->get('done');
        $tag = $new->get('tag');

        Task::where('id', $id)->update(['name' => $name, 'description' => $description, 'done' => $done, 'tag' => $tag]);

        return response()->json('Succesfully updated the data!');
    }
}
