<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class IsAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $requestApiKey = $request->header('TODO-APIKEY');
        $presetApiKey = Config::get('app.api');

        

        if(isset($requestApiKey)){
            
            if($requestApiKey === $presetApiKey){
                return $next($request);
                
            }
        }
        
        return response()->json(['status' => false, 'error' => 'INVALID API TOKEN'], 503);
    }
}
