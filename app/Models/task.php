<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'todo';

    protected $fillable = [
        'id',
        'name',
        'description',
        'done',
        'tag',
    ];

    protected $cast = [
        'done'=> 'boolean',
    ];
}
