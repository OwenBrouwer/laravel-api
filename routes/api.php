<?php

use Illuminate\Support\Facades\Route;
use App\Models\Task;

use App\Http\Controllers\TaskController;



Route::group(['middleware' => ['isAuthenticated']], function () { 

    Route::post('/create', [TaskController::class, 'create']);
    Route::get('/get', [TaskController::class, 'getTask']);
    Route::delete('/delete/{id}', [TaskController::class, 'delete']);
    Route::put('/update', [TaskController::class, 'update']);
    Route::get('/checked', [TaskController::class, 'checked']);
    Route::get('/unchecked', [TaskController::class, 'unchecked']);

  });